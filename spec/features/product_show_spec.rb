require 'rails_helper'

RSpec.feature 'ProductShowTest', type: :feature do
  given(:product) { create(:base_product) }

  scenario 'get Product#show' do
    visit potepan_product_url product.id
    expect(page).to have_selector 'h2', text: product.name
    expect(page).to have_selector 'h3', text: product.display_price
    expect(page).to have_selector 'p', text: product.description
  end
end

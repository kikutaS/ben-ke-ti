require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe '#full_title' do
    context 'when page_title is nil' do
      it { expect(full_title(nil)).to eq('BIGBAG Store') }
    end

    context 'when page_title is empty' do
      it { expect(full_title('')).to eq('BIGBAG Store') }
    end

    context 'when page_title is blank' do
      it { expect(full_title(' ')).to eq('BIGBAG Store') }
    end

    context 'when page_title is exist' do
      let(:product) { create(:base_product) }

      it { expect(full_title(product.name)).to eq("#{product.name} | BIGBAG Store") }
    end
  end
end

require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :request do
  let(:product) { create(:base_product) }

  describe '#show' do
    before do
      get potepan_product_url product.id
    end

    it 'successes request' do
      expect(response).to have_http_status 200
    end

    it 'includes the product' do
      expect(response.body).to include "#{product.name}"
      expect(response.body).to include "#{product.display_price}"
    end

    it 'renders the show template' do
      expect(response).to render_template :show
    end
  end
end
